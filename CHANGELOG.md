# Change Log for `epee` [2017-07-31]

## [2017-08-03]
+ **Updated**
    - refactor the `net` module

## [2017-08-01]
+ **Added**
    - examples for functions in namespace `epee::to_hex`
    - examples for `epee::misc_utils::median`
    - examples for `epee::console_handler`

## [2017-07-31]
+ **Added**
    - make a copy of `epee` library employed in `monero-project`
    - a demo for usage of `to_hex` struct
