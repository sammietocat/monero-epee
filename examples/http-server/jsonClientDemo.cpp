/**
*@file  clientDemo.cpp
*@brief     a utility class to interact with a specified server
*@author    sammietocat in json
*@version   2017-07-02
*/
#include <iostream>

#include "commons.hpp"

#include <boost/utility/value_init.hpp>

#include "net/http_client.h"
#include "string_tools.h"
#include "storages/http_abstract_invoke.h"

#include "Cmd.hpp"

using namespace epee;
using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    if(argc<2) {
        cerr << "too few arguments: missing key to query"<<endl;
        return 0x10;
    }

    TRY_ENTRY();

    string_tools::set_module_name_and_folder(argv[0]);
    cout << "module :"<<string_tools::get_current_module_name()<<endl;
    cout << "folder :"<<string_tools::get_current_module_folder()<<endl;

    const string srvIp = "127.0.0.1";
    const string srvPort = "8083";
    // make a proxy client to do the request
    net_utils::http::http_simple_client httpClient;
    if(!httpClient.connect(srvIp, srvPort, 1000)) {
        cerr << "failed to connect to the server"<<endl;
        return 0x11;
    }

    sCmd::request req = sCmd::request{argv[1]};
    sCmd::response resp;

    // ask the proxy client to send the request
    if(!net_utils::invoke_http_json_remote_command2("/onJsonHello", req, resp, httpClient)) {
        cerr << "failed to request from server"<<endl;
        return 0x12;
    }

    // display the response to console
    cout << "response for " <<req.info<<" is "<<endl;
    cout << "\tok: "<<resp.ok<<endl;
    cout << "\tfeedback: "<<resp.feedback<<endl;

    CATCH_ENTRY_L0("main", 1);
    return 0;
}