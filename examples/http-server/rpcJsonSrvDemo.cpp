/**
*@file  rpcJsonSrvDemo.cpp
*@brief     modeling a server accept request from clients
*@author    sammietocat
*@version   2017-07-02
*/
#include <iostream>

#include "HttpSrv.hpp"
#include "console_handler.h"

using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    TRY_ENTRY();

    string_tools::set_module_name_and_folder(argv[0]);
    cout << "module : "<<string_tools::get_current_module_name()<<endl;
    cout << "folder : "<<string_tools::get_current_module_folder()<<endl;

    HttpSrv srv;
    start_default_console(&srv, "#");

    const string srvIp = "127.0.0.1";
    //const string srvIp = "0.0.0.0";
    const string srvPort = "8083";

    if(!srv.bind2Address(srvIp, srvPort)) {
        cerr << "failed to bind with address  "<<srvIp<<":"<<srvPort<<endl;
        return 0x11;
    }
    cout << "now, listening on address "<<srvIp<<":"<<srvPort<<endl;

    if(!srv.start()) {
        cerr << "failed to start the server"<<endl;
        return 0x13;
    }
    cout << "now, the server is up"<<endl;

    char moreReq = 'y';
    while(srv.isRunning() && ('y'==moreReq)) {
        cout << "more request ? ('y' for yes) ";
        cin >> moreReq;
        cout <<endl;
    }

    if(!srv.timedWait(5)){
        cerr << "server stop unexpectedly"<<endl;
    }

    if(!srv.finalize()) {
        cerr << "failed to quit the server..."<<endl;
        return 0x12;
    }
    cout << "done..."<<endl;

    CATCH_ENTRY_L0("main", 1);
    return 0;
}