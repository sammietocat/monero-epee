/**
*@file  Cmd.hpp
*@brief     wrapper class for request and response for {@link HttpSrv} class
*@author    sammietocat
*@version   2017-07-02
*/
#pragma once

#include "serialization/keyvalue_serialization.h"
#include "storages/portable_storage_base.h"

namespace sammy
{
/**
    *@brief     a wrapper struct class for the request and response
    */
struct sCmd
{
    const static int ID = 2000;

    /**
        *@brief a request container
        */
    struct request
    {
        std::string info; //!< information as key for query

        BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(info)
        END_KV_SERIALIZE_MAP()
    };

    /**
    *@brief     a response container
    */
    struct response
    {
        bool ok;              //!< status of response
        std::string feedback; //!< message feedback

        BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(ok)
        KV_SERIALIZE(feedback)
        END_KV_SERIALIZE_MAP()
    };
};
}