#include "HttpSrv.hpp"

namespace sammy {
/**
    *@brief     constructor of the class
    *@details   initialize the <code>running</code> flag as true, and add some
  *dummy data for test
    */
HttpSrv::HttpSrv()
    : running( false ) {
    // fill in some dummy data for testing
    db[ "hello" ] = "world";
    db[ "a" ]     = "b";
    db[ "loccs" ] = "sjtu";
}
/**
*@brief     de-constructor of the class
*/
HttpSrv::~HttpSrv() {}
/**
*@brief     bind the current server to a specified address
*@details   the actual job is done through the proxy object <code>hdl</code>,
            which will set this object as the callback handler for
            <code>hdl</code>, and delegate the initialization job to
            the <code>init_server</code> of <code>hdl</code>
*@param ip  the ip address to listen
*@param port    the port to listen
*@return    <code>true</code> if initialization is ready, and false otherwise
*/
bool HttpSrv::bind2Address( const string &ip, const string &port ) {
    // set self as the callback handler
    hdl.get_config_object().m_phandler = this;
    // set folder for hosting requests
    hdl.get_config_object().m_folder = "";
    // bind the handler to the specified ip and port
    return hdl.init_server( port, ip );
}
/**
*@brief     bootstrap the server
*@return    <code>true</code> if the underlying <code>hdl</code> succeed to
            run up, and false otherwise
*/
bool HttpSrv::start() {
    running = true;
    // set the working thread
    int thCnt = 4;

    if ( !hdl.run_server( thCnt, false ) ) {
        cerr << "failed to run net tcp server" << endl;
        return false;
    }

    return true;
}
/**
*@brief     get the running status of the server
*@return    <code>true</code> if the server is running, and false otherwise
*/
bool HttpSrv::isRunning() { return running; }
/**
*@brief     wait the underlying thread for serving jobs for a specific time
*@param seconds the maximum seconds to wait before quit
*@return <code>true</code> return due to joining of all threads, and
            <code>false</code otherwise
*/
bool HttpSrv::timedWait( const int seconds ) {
    return hdl.timed_wait_server_stop( seconds );
}
/**
*@brief clean up the server
*@details   simply delegated the job the <code>deinit_server</code>
            underlying <code>hdl</code> object
*@return    <code>true</code> if no error occurred, and <code>false</code>
            otherwise
*/
bool HttpSrv::finalize() { return hdl.deinit_server(); }
/**
*@brief     tell the server to stop running
*@details   set the <code>running<code> flag to false, and ask the underlying
            <code>hdl</code> object to call <code>send_stop_signal()</code>
            for stopping
*@return    <code>true</code> if signaling is done well, and <code>false</code>
            otherwise
*/
bool HttpSrv::send_stop_signal() {
    running = false;
    hdl.send_stop_signal();
    return true;
}

/**
*@brief a callback function for a rpc request
*@details   based on the key contained in the request, query the underlying
            database for the corresponding value, and populate the response
field
            with
            + a "true" state and a "found" message in case of existing key
            + a "true" state and a "not found" message in case of non-existing
key
*@return <code>true</code> if the request handling process is ok, and
        <code>false</code> otherwise
*@note  its signature must take form as the declared one
*/
bool HttpSrv::onSayHello( const sCmd::request &req, sCmd::response &resp,
                          connCtx &ctx ) {
    cout << "handling request in onSayHello..." << endl;
    cout << "key got is " << req.info << endl;

    resp.ok       = true;
    resp.feedback = "Gotcha :)";
    auto v        = db.find( req.info );
    if ( db.end() != v ) {
        resp.feedback += " We found " + v->second;
    } else {
        resp.feedback += " Sorry, but we found no such key";
    }
    resp.feedback += " for you";

    cout << "done handling request in onSayHello..." << endl;
    cout << "resp.feedback = " << resp.feedback << endl;
    // quit

    return true;
}
bool HttpSrv::onCannotSayHello( const sCmd::request &req, sCmd::response &resp,
                                epee::json_rpc::error &errResp, connCtx &ctx ) {
    errResp.code    = 404;
    errResp.message = "bla bla bla...";

    return false;
}
};