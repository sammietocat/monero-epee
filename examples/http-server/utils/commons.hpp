/**
*@file  commons.hpp
*@brief     a common heade file defining some useful macros and include a header file for logging
*@author    sammietocat
*@version   2017-07-02
*/
#pragma once

#define BOOST_FILESYSTEM_VERSION 3
#define ENABLE_RELEASE_LOGGING
#include "misc_log_ex.h"