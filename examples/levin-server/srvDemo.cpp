//
// Created by loccs on 7/21/17.
//

#include <iostream>

#include "string_tools.h"
#include "misc_log_ex.h"

#include "LevinSrv.hpp"

using namespace epee;
using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    // macro defined in "misc_log_ex.h"
    //TRY_ENTRY()

    try {
        // record the running context
        string_tools::set_module_name_and_folder(argv[0]);


        cout << "Demo server starting ..." << endl;

        // IP and port to bind for server
        //const string IP = "127.0.0.1";
        const string IP = "0.0.0.0";
        const string PORT = "9090";

        LevinSrv srv;
        if (!srv.bind2Address(IP, PORT)) {
            cerr << "server cannot bind to " << IP << ":" << PORT << endl;
            return 0x10;
        }
        cout << "Now, server is listening on "<<IP<<":"<<PORT<<endl;

        cout << "try to start the server" << endl;
        if (!srv.start()) {
            cerr << "fail to bootstrap the server :(" << endl;
            return 0x11;
        }
        cout << "Server is now up"<<endl;

        char more = 'y';
        while ('y'==more) {

            /*
            enu::connection_context_base connCtx = boost::value_initialized<decltype(connCtx)>();
            enu::boosted_levin_async_server worker = srv.getWorker();
            if(!srv.getWorker().connect(IP,PORT,10000, connCtx)) {
                cerr << "connection failure"<<endl;
                return 0x13;
            }
             */

            srv.communicate();
            srv.communicateAsync();

            cout << "************************"<<endl;
            cout << "More? ";
            cin >> more;
            cout <<endl;
        }

        srv.stop();
        srv.finalize();
    } catch (const std::exception &ex) {
        cerr << "Exception at main: " << ex.what() << endl;
        return 0x12;
    } catch (...) {
        cerr << "Exception at main: generic exception ..." << endl;
        return 0x13;
    }
    //CATCH_ENTRY_L0("main", 1)

    return 0;
}