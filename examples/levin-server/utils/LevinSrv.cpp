//
// Created by loccs on 7/21/17.
//

#include "LevinSrv.hpp"

namespace sammy {
    LevinSrv::LevinSrv(const int thCnt) : THREAD_COUNT(thCnt) {
        worker.get_config_object().m_pcommands_handler = this;
    }

    LevinSrv::~LevinSrv() {}

    bool LevinSrv::bind2Address(const string &ip, const string &port) {
        return worker.init_server(port, ip);
    }

    bool LevinSrv::start() {
        worker.get_config_object().m_invoke_timeout = 10000;

        //cout << "start the server with "<<THREAD_COUNT<<" threads"<<endl;
        if(!worker.run_server(THREAD_COUNT, false)) {
            //cerr << "failed to bootstrap the server"<<endl;
            return false;
        }

        //cout << "quit..."<<endl;
        return true;
    }

    void LevinSrv::stop() {
        worker.send_stop_signal();
    }

    bool LevinSrv::finalize() {
        // stop the underlying worker
        return worker.deinit_server();
    }

    // private part
    /**
     * @brief   invoke
     * @note    equivalent to macro {@link epee::net_utils::CHAIN_LEVIN_INVOKE_MAP} if the callback
     *          function is {@link handle_invoke_map}
     * */
    int LevinSrv::invoke(int command, const std::string &in_buff, std::string &buff_out,
                         enu::connection_context_base &context) {
        bool handled = false;
        return handle_invoke_map(false, command, in_buff, buff_out, context, handled);
    }

    /**
     * @brief   just make a dummy notify function
     * @note equivalent to macro {@link epee::net_utils::CHAIN_LEVIN_NOTIFY_STUB}
     * */
    int LevinSrv::notify(int command, const std::string &in_buff, enu::connection_context_base &context) {
        return -1;
    }

    /**
     * @brief   handle a given command
     * @param is_notify effect is unknown
     * @param command   format ID of command
     * @param in_buff   input byte string as query parameter
     * @param buff_out  byte string as output after handling
     * @param context   current connection conext
     * @param handled   whether the command has been handled by some node in the chain
     * @return  a status code, where 0 means successful handling, 0xff means no handling
     * */
    template<class t_context>
    int LevinSrv::handle_invoke_map(bool is_notify, int command, const std::string &in_buff, std::string &buff_out,
                                    t_context &context, bool &handled) {
        if (!is_notify && sCmd::ID == command) {
            handled = true;
            return enu::buff_to_t_adapter<LevinSrv, sCmd::request, sCmd::response>(command, in_buff, buff_out,
                                                                                     boost::bind(&LevinSrv::sayHello, this,
                                                                                                 _1, _2, _3, _4),
                                                                                     context);
        }

        return 0xff;
    }

    // handlers
    int LevinSrv::sayHello(int cmdId, sCmd::request &req, sCmd::response &resp,
                           const enu::connection_context_base &ctx) {
        resp.ok = true;
        resp.feedback = "Hello " + req.info;

        return 1;
    }
    int LevinSrv::wakeUp(int cmdId, sCmd::request &req, const enu::connection_context_base &ctx) {
        cout << "Message '"<<req.info<<"' wake me up..."<<endl;
        return 1;
    }
}