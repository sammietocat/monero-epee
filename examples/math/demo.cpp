/**
 * @file demo.cpp
 * @author sammietocat
 * @brief   running tests for the {@link misc_utils::median(vector<T>)} function
 * */
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "misc_language.h"

using namespace epee;
using namespace std;

/**
 * @brief print a vector surrounded by "[]"
 * @param ivec vector to print
 * */
void printVec(const vector<int> &ivec) {
    cout << "[";
    if (!ivec.empty()) {
        cout << ivec[0];
        for (size_t i = 1; i < ivec.size(); ++i) {
            cout << "," << ivec[i];
        }
    }
    cout << "]" << endl;
}

void runTest(vector<int> ivec, const int &expectedMedian) {
    cout << "*** test case ***" << endl;

    cout << "\toriginal vector is ";
    printVec(ivec);

    // !!!WARNING: this function will modify the given vector
    int m = misc_utils::median(ivec);

    cout << "\tvector in order is ";
    printVec(ivec);

    cout << "\texpected median is " << expectedMedian << endl;
    cout << "\toutput median is " << m << endl;

    cout << "\tHENCE, misc_utils::median() IS " << (expectedMedian == m ? "" : "NOT") << " OK" << endl;
}

int main(int argc, char *argv[]) {
    const vector <vector<int>> vec2d = {
            {},
            {298},
            {277,  159},
            {571,  741, 886},
            {87,   391, 97,  62},
            {295,  935, 949, 729, 53},
            {1005, 817, 193, 196, 946, 680},
            {818,  256, 435, 498, 206, 478, 746},
            {35,   901, 792, 333, 154, 951, 905, 896}
    };
    const vector<int> M = {0, 298, 218, 741, 92, 729, 748, 478, 844};

    for (size_t i = 0; i < vec2d.size(); ++i) {
        runTest(vec2d[i], M[i]);
    }

    return 0;
}
