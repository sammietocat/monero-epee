/**
 * @file    hex-demo.cpp
 * @brief   demo of usage for utility functions in namespace {@link epee::to_hex}
 * @author  sammietocat
 * @version 2017-08-01
 * */
#include <iostream>

#include "hex.h"

using namespace std;

int main(int argc, char *argv[]) {
    uint8_t src[] = {0x00, 0x10, 0x11, 0x12, 0x13, 0x14, 0xff};

    // convert a byte sequence to a hex string
    cout << epee::to_hex::string(src) << endl;

    // insert the hex string of `src` into stdout
    epee::to_hex::buffer(std::cout, src);
    cout <<endl;

    // insert the formatted hex string of `src` into stdout
    epee::to_hex::formatted(std::cout, src);
    cout <<endl;

    return 0;
}