// Copyright (c) 2006-2013, Andrey N. Sabelnikov, www.sabelnikov.net
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of the Andrey N. Sabelnikov nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER  BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#pragma once
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

#include "string_tools.h"
namespace epee {
namespace net_utils {
namespace http {
/**
*@brief 	enumeration of http methods
*/
enum http_method {
    http_method_get,    ///< flag for http GET method
    http_method_post,   ///< flag for http POST method
    http_method_put,    ///< flag for http PUT method
    http_method_head,   ///< flag for http HEAD method
    http_method_etc,    ///< flag for http ETC method ???
    http_method_unknown ///< flag for unknown method
};
/**
*@brief 	enumeration of http Content-Type
*/
enum http_content_type {
    http_content_type_text_html, ///< text/html
    http_content_type_image_gif, ///< image/gif
    http_content_type_other,     ///< other
    http_content_type_not_set    ///< the type is not set
};
/**
* a type definition for the field list in the query string
* which is a list of string pair
*/
typedef std::list<std::pair<std::string, std::string>> fields_list;

/**
*@brief 	retrieve the corresponding value for a given key among the given
field lists
*@param param_name 	key to find value for
*@param fields field list consists of a list of (key,value) pairs
*@return 	the corresponding value for the given key specified in `param_name`
in case of the key exists
            otherwise an empty string
*/
inline std::string
    get_value_from_fields_list( const std::string &                 param_name,
                                const net_utils::http::fields_list &fields ) {
    fields_list::const_iterator it = fields.begin();
    for ( ; it != fields.end(); it++ )
        if ( !string_tools::compare_no_case( param_name, it->first ) ) break;

    if ( it == fields.end() ) return std::string();

    return it->second;
}

inline std::string get_value_from_uri_line( const std::string &param_name,
                                            const std::string &uri ) {
    std::string buff = "([\\?|&])";
    buff += param_name + "=([^&]*)";
    boost::regex match_param( buff.c_str(),
                              boost::regex::icase | boost::regex::normal );
    boost::smatch result;
    if ( boost::regex_search( uri, result, match_param,
                              boost::match_default ) &&
         result[ 0 ].matched ) {
        return result[ 2 ];
    }
    return std::string();
}

/**
*@brief 	a container for http header data
*/
struct http_header_info {
    std::string m_connection;        ///< "Connection:"
    std::string m_referer;           ///< "Referer:"
    std::string m_content_length;    ///< "Content-Length:"
    std::string m_content_type;      ///< "Content-Type:"
    std::string m_transfer_encoding; ///< "Transfer-Encoding:"
    std::string m_content_encoding;  ///< "Content-Encoding:"
    std::string m_host;              ///< "Host:"
    std::string m_cookie;            ///< "Cookie:"
    fields_list m_etc_fields;

    /**
    *@brief	reset the all field in the header to their default value
    *@details 	default value for string field is "", and `fields_list` is
    *empty list
    */
    void clear() {
        m_connection.clear();
        m_referer.clear();
        m_content_length.clear();
        m_content_type.clear();
        m_transfer_encoding.clear();
        m_content_encoding.clear();
        m_host.clear();
        m_cookie.clear();
        m_etc_fields.clear();
    }
};

/**
*@brief 	container for the information in uri, including path, query,
*fragment and query params
*/
struct uri_content {
    std::string m_path;     //!< path of host
    std::string m_query;    //!< query string
    std::string m_fragment; //!< fragment
    std::list<std::pair<std::string, std::string>>
        m_query_params; //!< pairs of query parameters in form of {(key,value)}
};

/**
*@brief 	container of the url content, including scheme, host, uri, port and
*{@link uri_content}
*/
struct url_content {
    std::string schema;        //!< schema, such as "http", "https"
    std::string host;          //!< path/name of host
    std::string uri;           //!< string form of <code>m_uri_content<code>
    uint64_t    port;          //!< port of host
    uri_content m_uri_content; ///< detailed as {@link uri_content}
};
/**
*@brief 	container for information of the http request
*/
struct http_request_info {
    http_request_info()
        : m_http_method( http_method_unknown )
        , m_http_ver_hi( 0 )
        , m_http_ver_lo( 0 )
        , m_have_to_block( false ) {}

    http_method
                m_http_method; ///< type of method, specified as {@link http_method}
    std::string m_URI;         ///< uri string
    std::string m_http_method_str;  ///< string-version of http method
    std::string m_full_request_str; ///< the full string of the request
    std::string m_replace_html;     ///< ???
    std::string m_request_head;     ///< header of the request
    int         m_http_ver_hi;      ///< major version tag of the http
    int         m_http_ver_lo;      ///< minor version tag of the http
    bool        m_have_to_block;
    http_header_info
                m_header_info; ///< header container as {@link http_header_info}
    uri_content m_uri_content; ///< uri content container as {@link uri_content}
    size_t      m_full_request_buf_size; ///< buffer size of the full request
    std::string m_body;                  ///< content of the body

    /**
    *@brief 	clean up this object, and make a new one
    */
    void clear() {
        this->~http_request_info();
        new ( this ) http_request_info();
    }
};

/**
*@brief 	container of the http response
*/
struct http_response_info {
    int              m_response_code;     ///< status code, e.g., 404
    std::string      m_response_comment;  ///< extra comment
    fields_list      m_additional_fields; ///< extra fields
    std::string      m_body;              ///< content of body
    std::string      m_mime_tipe;         ///< MIME type
    http_header_info m_header_info; ///< header info as {@link http_header_info}
    int              m_http_ver_hi; ///< OUT paramter only
    int              m_http_ver_lo; ///< OUT paramter only
                                    /**
                                    *@brief 	reset this object
                                    */
    void clear() {
        this->~http_response_info();
        new ( this ) http_response_info();
    }
}; // end struct http_response_info
} // end namespace http
} // end namespae net_utils
} // end namespace epee