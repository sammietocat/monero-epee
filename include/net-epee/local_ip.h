// Copyright (c) 2006-2013, Andrey N. Sabelnikov, www.sabelnikov.net
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of the Andrey N. Sabelnikov nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER  BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

/**
 * @file local_ip.h
 * @brief   a set of interfaces to check if a given ip is local or loop back
 * */

#pragma once

namespace epee {
    namespace net_utils {
        /**
         * @brief   check if a given ip address is a local address
         * @details  addresses falls within following range are treated as local address,
         *              + 10.0.0.0    - 10.255.255.255
         *              + 172.16.0.0  - 172.31.255.255
         *              + 192.168.0.0 - 192.168.255.255
         * @return  `true` if the address is local, and `false` otherwise
         * @note the address specified by the unsigned 32-bit integer is interpreted in little endian, i.e.,
         *          given ip=0x11223344, the address will be "68.51.34.17"
         * */
        inline bool is_ip_local(uint32_t ip) {
            if ((ip | 0xffffff00) == 0xffffff0a)
                return true;

            if ((ip | 0xffff0000) == 0xffffa8c0)
                return true;

            if ((ip | 0xffffff00) == 0xffffffac) {
                uint32_t second_num = (ip << 8) & 0xff000000;
                if (second_num >= 16 && second_num <= 31)
                    return true;
            }
            return false;
        }

        /**
         * @brief   check if a given ip address is a loopback address
         * @details  addresses falls within following range are treated as local address,
         *              + 127.0.0.0 - 127.255.255.255
         * @return  `true` if the address is a loop back address, and `false` otherwise
         * @note the address specified by the unsigned 32-bit integer is interpreted in little endian, i.e.,
         *          given ip=0x11223344, the address will be "68.51.34.17"
         * */
        inline
        bool is_ip_loopback(uint32_t ip) {
            if ((ip | 0xffffff00) == 0xffffff7f)
                return true;
            return false;
        }

    } //namespace net_utils
} // namespace epee

